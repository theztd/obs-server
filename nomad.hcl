variable "dcs" {
    type = list(string)
    default = ["dc1", "dev"]
}

variable "fqdn" {
    type = string
}

variable "image" {
  type = string
}

job "__JOB_NAME__" {
  region = "global"

  datacenters = var.dcs

  type = "service"

  group "player" {

    network {

      port "web" {
        to = 80
      }

      port "rtmp" {
        to = 1935
        static = 1935
      }
    }

    service {
      port = "web"

      tags = [
            "public",
            "traefik.enable=true",
            "traefik.http.routers.${NOMAD_JOB_NAME}-http.rule=Host(`${var.fqdn}`)"
      ]
#            I use cloudflare for https      
#            "traefik.http.routers.${NOMAD_JOB_NAME}-http.tls=true"
#      ]

      check {
        type = "http"
        path = "/"
        interval = "10s"
        timeout = "2s"
      }
    }

    service {
      port = "rtmp"

      // check {
      //   type = "tcp"
      //   port = 1935
      //   interval = "10s"
      // }

      // tags = [
      //       "public",
      //       "traefik.enable=true",
      //       "traefik.tcp.routers.${NOMAD_JOB_NAME}-rtmp.rule=HostSNI(`stream.fejk.net`)",
      //       "traefik.tcp.routers.${NOMAD_JOB_NAME}-rtmp.entrypoints=rtmp",
      //       "traefik.tcp.routers.${NOMAD_JOB_NAME}-rtmp.service=player-main-player",
      //       "traefik.tcp.services.${NOMAD_JOB_NAME}-rtmp.loadbalancer.server.port=1935"
      // ]

      // tags = [
      //   "public",
      //   "traefik.enable=true",
      //   "traefik.http.routers.${NOMAD_JOB_NAME}-rtmp.rule=Host(`stream.fejk.net`)", # Connect ant media to traefik
      //   "traefik.http.services.${NOMAD_JOB_NAME}-rtmp.loadbalancer.passhostheader=true", # Enable pass host header
      //   "traefik.http.services.${NOMAD_JOB_NAME}-rtmp.loadbalancer.server.port=5080"
      // ]

    }


    task "nginx" {
      driver = "docker"

      config {
        image = var.image
        ports = ["web"]
        force_pull = true

        volumes = [
          "local/nginx.conf:/etc/nginx/nginx.conf-DISABLED"
        ]
        
      }

      template {
        data = file("./nginx.conf")
        destination = "local/nginx.conf"
        perms = "0644"
      }

      resources {
        cpu = 100
        memory = 16
      }

    }

  }
}
