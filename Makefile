test:
	cat nomad.hcl | sed "s/__JOB_NAME__/test/g" > nomad-run.hcl
	nomad job run -var="dcs=[\"dc1\"]" -var="fqdn=test-latest.fejk.net" -var="image=registry.gitlab.com/theztd/obs-server:main" nomad-run.hcl