FROM vallahaye/nginx-rtmp


COPY nginx.conf /etc/nginx/nginx.conf
COPY player/ /usr/share/nginx/html/player/

EXPOSE 1935
EXPOSE 8081
EXPOSE 80
